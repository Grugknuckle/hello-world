let express = require('express')
let app = express()
let opn = require('opn')

const port = 5000
app.get('/', function (req, res) {
  res.send('Hello World! Mike Bruno is the Boss.')
})

app.listen(port, function () {
  console.log('Example app listening on port' + port + '!')
  opn('http://localhost:' + port)
})